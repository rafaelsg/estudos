/**
 * Arquivo de configuração de tarefas automatizadas
 * @author Rafael Soares (rafaelsoares01@gmail.com)
 * @version 1.0
 * @copyright Rafael Soares 2016
 */

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // Watch
    watch: {
      files: ['assets/dev/css/*.scss', 'assets/dev/js/*.js'],
      tasks: ['concat', 'uglify', 'sass', 'cssmin']
    },
    // Concatena todos os CSS e JS
    concat: {
      js: {
        src: 'assets/dev/js/*.js',
        dest: 'assets/js/scripts.js'
      },
      css: {
        src: 'assets/dev/css/*.scss',
        dest: 'assets/css/style.scss'
      }
    },
    // Minifica o JS
    uglify: {
      options: {
        banner: '/* Scoop Tecnologia - IPREDE */\n'
      },
      build: {
        src: 'assets/js/scripts.js',
        dest: 'assets/js/scripts.min.js'
      }
    },
    // SASS
    sass: {
      dist: {
        options: {style:'compressed'},
        files: {
          'assets/css/style.css' : 'assets/css/style.scss'
        }
      }
    },
    cssmin: {
      css: {
        src: 'assets/css/style.css',
        dest: 'assets/css/style.min.css'
      }
    },
  });
  
  // Plugins do Grunt
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');

  // Tarefas que serão executadas
  grunt.registerTask('default', ['concat', 'uglify', 'sass', 'cssmin']);
  grunt.registerTask('w', ['watch']);
}